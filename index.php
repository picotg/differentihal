<?php
/**
 * Utilitaires pour comparer 2 requête sur Hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * DiferentiHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

session_start();
require('halRequestBuilder.php');
require('clean.php');
cleanOldFile();

// création de la clés personnelle
if(!isset($_SESSION['personalKey'])) {
    $_SESSION['personalKey'] = bin2hex(random_bytes(20));
}
$personalKey = $_SESSION['personalKey'];

// récupération paramètre de requête
$request1 = isset($_GET['request1'])?$_GET['request1']:'';
$request2 = isset($_GET['request2'])?$_GET['request2']:'';
$fields = isset($_GET['fields'])?$_GET['fields']:'';
$requestName1 = isset($_GET['requestName1'])?$_GET['requestName1']:'';
$requestName2 = isset($_GET['requestName2'])?$_GET['requestName2']:'';
?>
<!DOCTYPE>
<html>
    <head>
        <title>Différence entre 2 requêtes</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.3/css/bulma.min.css">
    </head>
<body class="content">
<nav class="navbar" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a class="navbar-item" href="index.php">
            <img src="img/logo.jpg" height="28">
        </a>
    </div>
    <div class="navbar-menu">
        <div class="navbar-start">
            <a class="navbar-item">
                DifferentiHAL
            </a>
        </div>
    </div>
</nav>
<div class="container">
    
    <h1 class="title">Différence entre 2 requêtes</h1>

    <h2 class="subtitle">Description et fonctionnement</h2>

    <p>Ce script permet de faire 2 requêtes sur Hal en parallèle et de créer quatre fichiers CSV contenant le résultat de ces requêtes ainsi que les différences.</p>

    <p>Les champs requête 1 et requête 2 doivent recevoir des requêtes de recherche Hal. (Voir la documentation : <a href="https://api.archives-ouvertes.fr/docs/search">https://api.archives-ouvertes.fr/docs/search</a>). Les paramètres fl et start ne sont pas utilisables. Pour exécuter la requête jusqu'au bout, ce script utilise les curseurs (<a href="https://api.archives-ouvertes.fr/docs/search/?#cursors">https://api.archives-ouvertes.fr/docs/search/?#cursors</a>). Le paramètre fl est commun aux deux requêtes et correspond à la valeur des Champs à afficher.</p>

    <p>L'exécution du script produit quatre fichiers, nommés d'après les champs "Nom requête 1" et "Nom requête 2" du formulaire :
        <ul>
            <li>le fichier "Nom requête 1".csv : contenant les résultats trouvés par la requête 1</li>
            <li>le fichier "Nom requête 2".csv : contenant les résultats trouvés par la requête 2</li>
            <li>le fichier "Nom requête 1"-"Nom requête 2".csv : contenant les résultats trouvés par la requête 1 mais pas par la requête 2</li>
            <li>le fichier "Nom requête 2"-"Nom requête 1".csv : contenant les résultats trouvés par la requête 2 mais pas par la requête 1</li>
        </ul>
    </p>

    <h2 class="subtitle">Requête</h2>
    <form action="" method="GET">
        <label class="label" for="request1">Requête 1</label>
        <input class="input" id="request1" name="request1" type="text" style="width: 100%;" value="<?= htmlspecialchars($request1) ?>"><br />
        <label class="label" for="requestName1">Nom requête 1 : </label>
        <input class="input" id="requestName1" name="requestName1" type="text" style="width: 100%;" value="<?= $requestName1 ?>"><br />
        <label class="label" for="request2">Requête 2 : </label>
        <input class="input" id="request2" name="request2" type="text" style="width: 100%;" value="<?= htmlspecialchars($request2) ?>"><br />
        <label class="label" for="requestName2">Nom requête 2 : </label>
        <input class="input" id="requestName2" name="requestName2" type="text" style="width: 100%;" value="<?= $requestName2 ?>"><br />
        <label class="label" for="fields">Champs à afficher (séparés par une virgule) : </label>
        <input class="input" id="fields" name="fields" type="text" value="<?= $fields ?>"><br />
        <input class="button is-primary" type="submit" >
    </form><br /><br />
<?php

/**
 * génère les fichiers temporaires
 *
 * @param HalRequestIterator $a itérateur sur la requête numéro 1
 * @param HalRequestIterator $b itérateur sur la requête numéro 2
 * @param array $fields champs à renvoyer dans le fichier
 * @param string $key clé unique pour créer des fichiers différents pour chaque utilisateur
 * @return array tableau des noms de fichier
 */
function paralelle(HalRequestIterator $a, HalRequestIterator $b, array $fields, string $key) {
    // nom des fichers générés
    $filenames = [
        'a' => $a->getName(),
        'b' => $b->getName(),
        'a-b' => $a->getName().'-'.$b->getName(),
        'b-a' => $b->getName().'-'.$a->getName(),
    ];
    // accès aux fichiers générés
    $files = [
        'a' => fopen('tmp/'.$a->getName().$key.'.csv', 'w'),
        'b' => fopen('tmp/'.$b->getName().$key.'.csv', 'w'),
        'a-b' => fopen('tmp/'.$a->getName().'-'.$b->getName().$key.'.csv', 'w'),
        'b-a' => fopen('tmp/'.$b->getName().'-'.$a->getName().$key.'.csv', 'w'),
    ];

    // ajout en-tête BOM UTF-8 à chaque fichier CSV pour amélioration compatibilité avec excel
    foreach($files as $key => $file) fwrite($file, "\xEF\xBB\xBF");
    // itération sur chaque résultat document renvoyé par la requête a
    foreach($a as $doc_a) {
        // itération jusqu'à trouver le document courant dans la requête b ou atteindre la fin d'une requête
        while(true) {
            if(!$b->valid()) break;
            $doc_b = $b->current();
            if($doc_a == null) break;
            if($doc_b == null) break;
            $fields_value = [
                'a' => [],
                'b' => []
            ];
            foreach($fields as $field) {
                $values = [
                    'a' => isset($doc_a->$field)?$doc_a->$field:"",
                    'b' => isset($doc_b->$field)?$doc_b->$field:""
                ];
                foreach($values as $queryName => $value) {
                    if(is_array($value)) {
                        $temp = '';
                        foreach($value as $index => $element) {
                            if($index!=0) $temp .= ', ';
                            $temp .= utf8_encode(utf8_decode($element));
                        }
                        $values[$queryName] = $temp;
                    }
                }
                array_push($fields_value['a'], $values['a']);
                array_push($fields_value['b'], $values['b']);
            }
            if($doc_a->docid==$doc_b->docid) {
                fputcsv($files['a'], $fields_value['a'], $separator = ";");
                fputcsv($files['b'], $fields_value['b'], $separator = ";");
                $b->next();
                break;
            } elseif($doc_a->docid>$doc_b->docid) {
                fputcsv($files['b-a'], $fields_value['b'], $separator = ";");
                fputcsv($files['b'], $fields_value['b'], $separator = ";");
            } else {
                fputcsv($files['a-b'], $fields_value['a'], $separator = ";");
                fputcsv($files['a'], $fields_value['a'], $separator = ";");
                break;
            }
            $b->next();
        }
    }
    foreach($files as $key => $file) fclose($file);
    return $filenames;
}

/*
 * vérification de la présence des champs requie dans les paramètres de la requête get
 */
if(isset($_GET['request1'])&&isset($_GET['request2'])&&isset($_GET['fields'])&&isset($_GET['requestName1'])&&isset($_GET['requestName2'])) {
    if(strpos($fields, 'docid')===false) $fields = 'docid,'.$fields;
    $request1 = str_replace(' ', '%20', $request1);
    $request2 = str_replace(' ', '%20', $request2);
    $full_request1 = $request1.'&fl='.$fields;
    $request1 = new HalRequestIterator($full_request1, $requestName1);
    $full_request2 = $request2.'&fl='.$fields;
    $request2 = new HalRequestIterator($full_request2, $requestName2);
    $fields = explode(',', $fields);
    $filenames = paralelle($request1, $request2, $fields, $personalKey);
    ?>
<h2>Résultats</h2>
<?php foreach($filenames as $filename):?>
<a href="csv.php?filename=<?= $filename ?>">téléchager <?= $filename?></a> <br/>
<?php endforeach; ?>
    <?php
}

?>
</div>
</body>
</html>
