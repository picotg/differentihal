<?php
/**
 * téléchargement de fichier CSV
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * DiferentiHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

session_start();

$personalKey = $_SESSION['personalKey'];
$filename = $_GET['filename'];

header('Content-Type: text/csv');
header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
header('Content-disposition: attachment;filename='.$filename.'.csv');

echo file_get_contents('tmp/'.$filename.$personalKey.'.csv');
