<?php
/**
 * Utilitaires pour facilité la génération de requête pour l'API Hal
 * 
 * @author Gaël PICOT <gael.picot@univ-grenoble-alpes.fr>
 * 
 * DiferentiHal :
 * Copyright (C) 2022 UGA
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>.
 * 
 */

 /**
  * class permetant de selectionné un type de document parmis le reférenciel hal doctype.
  */
class DocTypeSelector {
    /**
     * @var stdClass donnée représentant le résultat d'une requête sur le reférenciel hal doctype.
     */
    private $data;
    /**
     * @var string nom du champs HTML.
     */
    private $HTMLNameField='doctype';
    /**
     * @var string label du champs HTML.
     */
    private $HTMLLabel = 'type de document :';

    /**
     * constructeur
     * 
     * construit les donnée ($this->data) à partir d'une requête sur le référenciel Hal ou
     * d'un fichier.
     *
     * @param ?string $portail portail hal
     * @param string $filename fichier de donée renvoyer par le référenciel doctype de Hal
     */
    public function __construct(?string $portail=null, string $filename="doctype_doi.json") {
        if ($portail==null) {
            $this->data = json_decode(file_get_contents($filename));
        } else {
            $query = 'https://api.archives-ouvertes.fr/ref/doctype';
            $query .= ($portail=='')?'':'?instance_s='.$portail;
            $this->data = json_decode(file_get_contents($query));
        }
    }

    /**
     * modifie la valeur de $this->HTMLNameField
     *
     * @param string $newValue
     * @return void
     */
    public function changeHTMLNameField(string $newValue) {
        $this->HTMLNameField = $newValue;
    }

    /**
     * génére le HTML d'un champs de type ComboBox
     *
     * @return string
     */
    public function generateHTMLComboBox() {
        $htmlComboBox = '<label for="'.$this->HTMLNameField.'">'.$this->HTMLLabel.' </label>';
        $htmlComboBox .= '<select name="'.$this->HTMLNameField.'" id="'.$this->HTMLNameField.'">';
        foreach($this->data->response->result->doc as $doctype) {
            $htmlComboBox .= '<option value="'.$doctype->str[0].'"';
            if(isset($_GET[$this->HTMLNameField]) && $_GET[$this->HTMLNameField] == $doctype->str[0]) {
                $htmlComboBox .= 'selected';
            }
            $htmlComboBox .= '>'.$doctype->str[1].'</option>';
        }
        $htmlComboBox .= '</select>';
        return $htmlComboBox;
    }
}

class PortailSelector {
    /**
     * @var stdClass donnée représentant le résultat d'une requête sur le reférenciel hal des portails (instance).
     */
    private $data;
    /**
     * @var string nom du champs HTML.
     */
    private $HTMLNameField='portail';
    /**
     * @var string label du champs HTML.
     */
    private $HTMLLabel = 'portail HAl :';
    /**
     * @var string label du champs HTML.
     */
    private $possibleNulValue = true;

    /**
     * constructeur
     * 
     * construit les donnée ($this->data) à partir d'une requête sur le référenciel Hal ou
     * d'un fichier.
     *
     * @param string $filename fichier de donée renvoyer par le référenciel doctype de Hal
     */
    public function __construct(string $filename='') {
        if ($filename!='') {
            $this->data = json_decode(file_get_contents($filename));
        } else {
            $query = 'https://api.archives-ouvertes.fr/ref/instance';
            $this->data = json_decode(file_get_contents($query));
        }
    }

    /**
     * génére le HTML d'un champs de type ComboBox
     *
     * @return string
     */
    public function generateHTMLComboBox() {
        $htmlComboBox = '<label for="'.$this->HTMLNameField.'">'.$this->HTMLLabel.' </label>';
        $htmlComboBox .= '<select name="'.$this->HTMLNameField.'" id="'.$this->HTMLNameField.'">';
        if($this->possibleNulValue) {
            $htmlComboBox .= '<option value="">aucun</option>';
        }
        foreach($this->data->response->docs as $instance) {
            $htmlComboBox .= '<option value="'.$instance->code.'"';
            if(isset($_GET[$this->HTMLNameField]) && $_GET[$this->HTMLNameField] == $instance->code) {
                $htmlComboBox .= 'selected';
            }
            $htmlComboBox .= '>'.$instance->name.'</option>';
        }
        $htmlComboBox .= '</select>';
        return $htmlComboBox;
    }

}

/**
 * iterateur sur une requête utilisant les curseurs
 */
class HalRequestIterator implements Iterator {
    private string $queryBase = '';
    private $results = [];
    private $needNewPage = false;
    private $nextCursorMark = '*';
    private $index = 0;
    private $lastPage = false;
    private $name = '';

    private function _addNextPage() {
        if(!$this->lastPage) {
            $requestResults = json_decode(file_get_contents($this->queryBase.$this->nextCursorMark));
            if(isset($requestResults->error)) {
                throw new Exception($requestResults->error);
            }
            $new_cursor = urlencode($requestResults->nextCursorMark);
            if($new_cursor == $this->nextCursorMark) {
                $this->lastPage = true;
            } else {
                $this->nextCursorMark = $new_cursor;
                foreach($requestResults->response->docs as $doc) {
                    array_push($this->results, $doc);
                }
            }
        }
    }

    public function __construct($query='', $name='', $sorteField='docid+asc')
    {
        $this->queryBase = $query.'&sort='.$sorteField.'&cursorMark=';
        $this->name = $name;
        $this->_addNextPage();
    }

    public function getResults() {
        return $this->results;
    }

    public function getName() {
        return $this->name;
    }

    public function rewind(): void {
        $this->index = 0;
    }

    public function current() {
        return $this->results[$this->index];
    }

    public function key() {
        return $this->index;
    }

    public function next():void {
        $this->needNewPage = count($this->results)<=$this->index+1;
        if(!$this->needNewPage||!$this->lastPage) {
            if($this->needNewPage) {
                $this->_addNextPage();
            }
            $this->index += 1;
        }
    }

    public function valid():bool {
        return !($this->needNewPage&&$this->lastPage);
    }
}

?>