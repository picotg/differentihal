<?php
const TIME_TO_LIVE = 3600;
function cleanOldFile() {
    $tmpFiles = scandir('tmp');
    foreach($tmpFiles as $tmpFile) {
        if($tmpFile!='.'&&$tmpFile!='..') {
            if(TIME_TO_LIVE <= time()-filemtime('tmp/'.$tmpFile)) {
                unlink('tmp/'.$tmpFile);
            }
        }
    }
}
?>