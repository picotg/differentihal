# differentiHal

Exécute et enregistre la différence entre 2 requêtes.

## fonctionnement

Ce script permet de faire 2 requêtes sur Hal en parallèle et de créer quatre fichiers CSV contenant le résultat de ces requêtes ainsi que les différences.

Les champs requête 1 et requête 2 doivent recevoir des requêtes de recherche Hal. ([Voir la documentation](https://api.archives-ouvertes.fr/docs/search)). Les paramètres fl et start ne sont pas utilisables. Pour exécuter la requête jusqu'au bout, ce script utilise les curseurs ([doc](https://api.archives-ouvertes.fr/docs/search/?#cursors)). Le paramètre fl est commun aux deux requêtes et correspond à la valeur des Champs à afficher.

L'exécution du script produit quatre fichiers, nommés d'après les champs "Nom requête 1" et "Nom requête 2" du formulaire :

- le fichier "Nom requête 1".csv : contenant les résultats trouvés par la requête 1
- le fichier "Nom requête 2".csv : contenant les résultats trouvés par la requête 2
- le fichier "Nom requête 1"-"Nom requête 2".csv : contenant les résultats trouvés par la requête 1 mais pas par la requête 2
- le fichier "Nom requête 2"-"Nom requête 1".csv : contenant les résultats trouvés par la requête 2 mais pas par la requête 1

## Description fichier

- clean.php : supprime les fichiers âgés de plus d'une heure. (exécution à chaque chargement du fichier index)
- csv.php : permet de rajouter les paramètres et de télécharger le fichier csv correspondant à la session courante.
- halRequestBuilder.php : permet de construire et d'exécuter une requête.
- index.php : script principal avec formulaire et création des fichier CSV temporaires.
